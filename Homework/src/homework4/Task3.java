package homework4;

public class Task3 {

	public static void main(String[] args) {
		int[][] array = { { 1, 2, 3, 4 }, { 1, 2, 3, 4 } };
		int sum = 0;
		float srednoaritmetichno;

		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				sum += array[row][col];
			}

		}
		srednoaritmetichno = sum / (array.length * array[0].length);
		System.out.println("Sumata na  elementite e " + sum + " Srednoto aritmetichno e " + srednoaritmetichno);

	}

}
