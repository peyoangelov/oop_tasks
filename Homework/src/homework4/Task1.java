package homework4;

import java.util.Scanner;

import javax.sound.midi.Synthesizer;

public class Task1 {

	public static void main(String[] args) {
		int[][] array = new int[6][5];

		Scanner sc = new Scanner(System.in);
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				System.out.println("Vavedete danni za red " + row + " kolona " + col);
				array[row][col] = sc.nextInt();
			}
		}
		int max = array[0][0];
		int min = array[0][0];
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				if (array[row][col] > max) {
					max = array[row][col];
				}
				if (array[row][col] < min) {
					min = array[row][col];
				}
			}

		}
		System.out.println("NAi-malkoto e " + min);
		System.out.println("Nai-golqmoto e " + max);

	}

}
