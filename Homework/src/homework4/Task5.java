package homework4;

public class Task5 {

	public static void main(String[] args) {
		int[][] array = {  { 1, 2, 3, 4 },  
							{ 5, 6, 7, 8 }, 
							{ 9, 10, 11, 12},  
							{ 13, 14, 15, 16 } };

		int maxRed=0;
		int maxColona=0;
		
		for(int row=0 ; row< array.length; row++){
			int sumRed=0;
			for(int col=0; col<array[0].length; col++){
				sumRed+= array[row][col];
			}
			if(sumRed > maxRed){
				maxRed = sumRed;
			}
			
		}
		for (int col=0; col <array[0].length; col++){
			int sumCol=0;
			for(int row=0 ; row < array.length;row ++){
				 sumCol+=array[row][col];
				 
			}
			if(sumCol> maxColona){
				maxColona=sumCol;
			}
			}
		System.out.println("Nai-golqma suma po redowe "+ maxRed);
		System.out.println("Nai-golqma suma po kolona "+ maxColona);
		if (maxColona> maxRed){
			System.out.println("Maksimalnata suma po coloni e po-golqma ot maksimalnata suma po redowe");
		}else {
		
			System.out.println("Maksimalnata suma po redowe e po-golqma ot maksimalnata suma po koloni");
		}
	}	

}
