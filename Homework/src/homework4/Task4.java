package homework4;

public class Task4 {

	public static void main(String[] args) {
		int[][] array2 = { { 1, 2, 3, 4 },  
				           { 5, 6, 7, 8 }, 
				           { 9, 10, 11, 12,},  
				           { 13, 14, 15, 16 } };

		int[][] rotate = new int[array2.length][array2[0].length];// row e array2.length = 4 ( 1,5,9,3)
																// col e array2[0].length = 4 ( 1,2,3,4)
		
		for (int row = 0; row < array2.length; row++) {
			for (int col = 0; col < array2[0].length; col++) {
				int index = col - (array2[0].length - 1);
				if (index < 0) {
					index *= -1;
				}

				rotate[row][col] = array2[index][row];
			}

		}
		for (int row = 0; row < rotate.length; row++) {
			for (int col = 0; col < rotate[0].length; col++) {
				System.out.print(rotate[row][col] + " ");
			}
			System.out.println();
		}

	}
}
