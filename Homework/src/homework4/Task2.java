package homework4;

import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		int[][] array = new int[4][4];
		Scanner sc = new Scanner(System.in);
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				array[row][col] = sc.nextInt();
			}
		}
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				if (row == col) {
					System.out.print(array[row][col] + " ");
				}

			}

		}
		System.out.println();
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				if (row + col == array.length - 1) {

					System.out.print(array[row][col] + " ");
				}
			}

		}
		
	}

}
