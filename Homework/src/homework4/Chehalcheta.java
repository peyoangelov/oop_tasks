package homework4;

public class Chehalcheta {

	public static void main(String[] args) {
		char[][] pod = { { '@', '*', '*', '*', '@', '@' }, { '*', '*', '@', '@', '@', '*' },
				{ '@', '*', '*', '*', '@', '@' }, { '*', '@', '@', '@', '*', '*' }, { '*', '*', '*', '*', '@', '@' },
				{ '*', '*', '*', '@', '*', '*' } };

		for (int day = 1; day <= 100; day++) {
			for (int row = 0; row < pod.length; row++) {
				for (int col = 0; col < pod[row].length; col++) {

					int countChehylcheta = 0;

					for (int r1 = row - 1; r1 <= row + 1; r1++) {
						if ((r1 >= 0) && (r1 < pod.length))
							for (int c1 = col - 1; c1 <= col + 1; c1++) {
								if ((c1 >= 0) && (c1 < pod[row].length)) {
									if ((r1 != row || c1 != col) && pod[r1][c1] == '@') {
										countChehylcheta++;
									}
								}
							}
					}

					if (pod[row][col] == '@') {
						if ((countChehylcheta < 2) || (countChehylcheta > 3)) {
							pod[row][col] = '*';
						}
					} else {
						if (countChehylcheta == 3) {
							pod[row][col] = '@';
						}
					}
				}
			}

			System.out.println("Day " + day);
			for (int row = 0; row < pod.length; row++) {
				for (int col = 0; col < pod[row].length; col++) {
					System.out.print(pod[row][col] + " ");
				}
				System.out.println();
			}
			System.out.println();
		}

	}

}
