package homework1;

import java.util.Scanner;

public class Task8 {

	public static void main(String[] args) {
		System.out.println("Enter number between 1000 and 9999");
		Scanner sc1 = new Scanner(System.in);
		int number = sc1.nextInt();

		int edinici = number % 10;
		int desetici = (number / 10) % 10;
		int stotici = (number / 100) % 10;
		int xilqdni = number / 1000;

		int newNumber1 = edinici + xilqdni * 10;
		int newNumber2 = desetici + stotici * 10;

		if (newNumber1 > newNumber2) {
			System.out.println(newNumber1 + ">" + newNumber2);
		}
		if (newNumber1 < newNumber2) {
			System.out.println(newNumber1 + "<" + newNumber2);
		}
		if (newNumber1 == newNumber2) {
			System.out.println(newNumber1 + "=" + newNumber2);

		}

	}

}
