package homework1;

import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		System.out.println("Please, enter first number");
		Scanner scanner = new Scanner(System.in);
		double firstInput = scanner.nextDouble();
		System.out.println("Please, enter second number");
		Scanner scanner2 = new Scanner(System.in);
		double secondInput = scanner2.nextDouble();
		System.out.println("Please, enter third number");
		Scanner scanner3 = new Scanner(System.in);
		double thirdInput = scanner3.nextDouble();

		double max;
		double min;

		if (firstInput > secondInput) {
			max = firstInput;
			min = secondInput;

		} else {
			max = secondInput;
			min = firstInput;
		}
		if (thirdInput < max && thirdInput > min) {
			System.out.println("Number " + thirdInput + "is between numbers " + min + " and " + max);

		} else {
			System.out.println("Number " + thirdInput + "is NOT between numbers " + min + " and " + max);
		}

	}

}
