package homework1;

import java.util.Scanner;

public class Task3 {

	public static void main(String[] args) {
		System.out.println("Please, enter first number");
		Scanner scanner1 = new Scanner(System.in);
		float firstInput = scanner1.nextFloat();
		System.out.println("Please, enter second number");
		Scanner scanner2 = new Scanner(System.in);
		float secondInput = scanner2.nextFloat();

		float temporary = firstInput;

		firstInput = secondInput;
		secondInput = temporary;
		System.out.println(firstInput + " " + secondInput);

	}

}
