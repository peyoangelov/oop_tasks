package homework1;

import java.util.Scanner;

public class Task12 {

	public static void main(String[] args) {
		System.out.println("Vavedete den");
		Scanner sc1 = new Scanner(System.in);
		int data = sc1.nextInt();
		System.out.println("Vavedete mesec");
		Scanner sc2 = new Scanner(System.in);
		int mesec = sc2.nextInt();
		System.out.println("Vavedete godini");
		Scanner sc3 = new Scanner(System.in);
		int godina = sc3.nextInt();

		byte den = 0, novMesec = 0;
		short novaGodina = (short) godina;
		if ((mesec <= 0 || mesec > 12) || (data <= 0 || data > 31)) {
			System.out.println("Vavedete mesec ot 1 do 12 i data ot 1 do 31");
		} else {
			boolean isAnyError = false;

			switch (mesec) {
			case 4:
			case 6:
			case 9:
			case 11:
				System.out.println("30 dni");
				if (data == 31) {
					System.out.println("Vavedeniqt mesec ima 30 dni");
					isAnyError = true;
				} else {
					if (data == 30) {
						den = 1;
						novMesec = (byte) (mesec + 1);

					} else {
						den = (byte) ((byte) data + 1);
						novMesec = (byte) mesec;
					}
				}
				break;
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:

				if (data == 31) {
					den = 1;
					novMesec = (byte) (mesec + 1);

				} else {
					den = (byte) ((byte) data + 1);
					novMesec = (byte) mesec;
				}
				if (mesec == 12) {
					den = 1;
					novMesec = 1;
					novaGodina = (short) (godina + 1);
				}
				break;
			case 2:
				if (data >= 30) {
					System.out.println("Mesec Fevruari ne moje da ima poveche ot 29 dni");
					isAnyError = true;
				} else if (godina % 400 == 0 || (godina % 4 == 0 && godina % 100 != 0)) {
					if (data == 29) {
						den = 1;
						novMesec = (byte) (mesec + 1);
					} else {
						den = (byte) ((byte) data + 1);
						novMesec = (byte) mesec;
					}
				} else {
					if (data == 28) {
						den = 1;
						novMesec = (byte) (mesec + 1);
					} else {
						den = (byte) ((byte) data + 1);
						novMesec = (byte) mesec;
					}

				}
			}
			if (!isAnyError)
				System.out.println(den + "," + novMesec + "," + novaGodina);

		}

	}

}
