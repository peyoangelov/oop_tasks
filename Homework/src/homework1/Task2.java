package homework1;

import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {

		System.out.println("Please, enter first number");
		Scanner scanner1 = new Scanner(System.in);
		float firstInput = scanner1.nextFloat();
		System.out.println("Please, enter second number");
		Scanner scanner2 = new Scanner(System.in);
		float secondInput = scanner2.nextFloat();

		float sum = firstInput + secondInput;
		System.out.println("The sum is " + sum);

		float razlika = firstInput - secondInput;
		System.out.println("Razlikata e " + razlika);

		float proizvedenie = firstInput * secondInput;
		System.out.println("Proizvedenieto e " + proizvedenie);

		float ostatak = firstInput % secondInput;
		System.out.println("Ostatakat ot delenieto e " + ostatak);
		
		float delenie = firstInput/secondInput;
		System.out.println("Rezultata ot delenieto e " + delenie);
		
	}
}
