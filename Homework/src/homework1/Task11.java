package homework1;

import java.util.Scanner;

public class Task11 {

	public static void main(String[] args) {
		System.out.println("Vavedete 3 cifreno chislo v koeto nqma 0");
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		int edinici = number % 10;
		int desetici = (number / 10) % 10;
		int stotici = (number / 100) % 10;

		if (edinici == 0 || desetici == 0 || stotici == 0) {
			System.out.println("Vavedete chislo v koeto nqma 0 !");
		} else {
			if (number % edinici == 0 && number % desetici == 0 && number % stotici == 0) {
				System.out.println("Deli se na vsichkite si cifri bez ostatak");
			} else {
				System.out.println("Ne se deli na vsichki");
			}

		}
	}

}
