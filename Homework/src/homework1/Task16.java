package homework1;

import java.util.Scanner;

public class Task16 {

	public static void main(String[] args) {
		System.out.println("Vavedete 3cifreno chislo");
		Scanner sc1 = new Scanner(System.in);
		int chislo = sc1.nextInt();

		int a = chislo / 100;
		int b = (chislo / 10) % 10;
		int c = chislo % 10;

		System.out.println(a + "  " + b + "  " + c);
		if (a == b && b == c) {
			System.out.println("Cifrite sa ravni");
		}
		else if (a > b && b > c) {
			System.out.println("Cifrite sa v nizhodqsht red");
		}
		else if (a < b && b < c) {
			System.out.println("Cifrite sa vav  vazhodqsht red");
		} else {
			System.out.println("Cifrite sa nenaredeni za opisanite sluchai");
		}
	}

}
