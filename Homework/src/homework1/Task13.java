package homework1;

import java.util.Scanner;

public class Task13 {

	public static void main(String[] args) {
		System.out.println("Enter temperature in celsius");
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();

		if (t <= -20) {
			System.out.println("Ledeno studeno");
		}
		if (t >= -19 && t <= 0) {
			System.out.println("Studeno");
		}
		if (t <= 15 && t >= 1) {
			System.out.println("Hladno");
		}
		if (t <= 25 && t >= 16) {
			System.out.println("Toplo");
		}
		if (t >= 26) {
			System.out.println("Goreshto");
		}

	}

}
