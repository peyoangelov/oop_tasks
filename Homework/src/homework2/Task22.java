package homework2;

import java.util.Scanner;

public class Task22 {

	public static void main(String[] args) {
		System.out.println("Vavedete chislo ot 1 do 999 ");
		Scanner sc = new Scanner(System.in);
		int chislo = sc.nextInt();
		int poredenNomer = 1;
		if (chislo >= 1 && chislo <= 999) {
			while (poredenNomer <= 10 && chislo <= 999) {
				chislo++;
				if (chislo % 2 == 0 || chislo % 3 == 0 || chislo % 5 == 0) {
					if ((poredenNomer == 10 || chislo == 999)) {
						System.out.print(poredenNomer + ":" + chislo);
					} else {
						System.out.print(poredenNomer + ":" + chislo + "; ");
					}
					poredenNomer++;
				}
			}

		} else {
			System.out.println("Vavedete chislo ot 1 do 999");
		}
	}

}
