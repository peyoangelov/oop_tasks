package homework2;

public class Tupalka {

	public static void main(String[] args) {
		// 0- kravata pecheli
		// 1 - kubrat pecheli
		// 2 - klicjko pecheli
		// 5 - nikoi
		byte[] resultati = new byte[12];

		for (int simulacia = 1; simulacia <= 12; simulacia++) {
			byte koiPecheli = 5;
			byte klichkoHealth = 100;
			byte kubratHealth = 100;
			boolean isKlichkoSick = false;
			boolean isKubratSick = false;
			byte numberOfDeadCow = (byte) ((Math.random() * 100) + 1);
			boolean isCowDead = false;
			System.out.println("*** Start of simulation " + simulacia + " ***");
			while ((klichkoHealth > 0) && (kubratHealth > 0)) {

				byte genchoNumber = (byte) ((Math.random() * 100) + 1);

				boolean isFirstBitTheSame = (genchoNumber & 1) == (numberOfDeadCow & 1);
				boolean isSecondBitTheSame = (genchoNumber & 2) == (numberOfDeadCow & 2);
				boolean isFifthBitTheSame = (genchoNumber & (1 << 4)) == (numberOfDeadCow & (1 << 4));

				if (!isCowDead && isFirstBitTheSame && isSecondBitTheSame && isFifthBitTheSame) {
					System.out.println("Bai Gencho - piqniq lovdjiq, zastrelq divata krava!");
					isCowDead = true;
				}

				if (!isCowDead) {
					if (Math.random() < 0.1) {
						System.out.println("Kravata gi smaza i dvamata");
						System.out.println("Macha go pecheli kravata!");
						koiPecheli = 0;
						klichkoHealth = 0;
						kubratHealth = 0;
						break;
					}

					if (Math.random() < 0.2) {
						System.out.println("Kravata blysna Kobrata");
						System.out.println("Macha go pecheli Klichko ama slujebno zaradi typata krava!");
						klichkoHealth = 0;
						kubratHealth = 0;
						koiPecheli = 2;
						break;
					}

					if (Math.random() < 0.2) {
						System.out.println("Kravata blysna Klichko");
						System.out.println("Macha go pecheli Kubrat ama slujebno zaradi typata krava!");
						klichkoHealth = 0;
						kubratHealth = 0;
						koiPecheli = 1;
						break;
					}
				}

				if ((!isKlichkoSick) && (Math.random() < 0.15)) { // Klichko se
																	// razbolqva
					isKlichkoSick = true;
					klichkoHealth /= 2;
					System.out.println("Klichko se razbolq. Zdraveto mu stana " + klichkoHealth);
				}

				if ((!isKubratSick) && (Math.random() < 0.15)) { // Kubrat se
																	// razbolqva
					isKubratSick = true;
					kubratHealth /= 2;
					System.out.println("Kubrat se razbolq. Zdraveto mu stana " + kubratHealth);
				}

				byte klichkoHit = (byte) ((Math.random() * 10) + 5);

				if (Math.random() < 0.33) { // 33% chance
					System.out.println("Klichko nameri shisheto s vodkata!");
					klichkoHit *= 2;
				}

				if (isKlichkoSick) {
					klichkoHit /= 2;
				}

				System.out.println("Klichko go prasna sys sila " + klichkoHit);
				kubratHealth = (byte) ((kubratHealth >= klichkoHit) ? kubratHealth - klichkoHit : 0);

				System.out.println("Na kubrat mu ostana " + kubratHealth + " kryv.");

				if (kubratHealth > 0) {
					byte kubratHit = (byte) ((Math.random() * 10) + 1);
					if (Math.random() < 0.33) { // 33% chance
						System.out.println("Kubrat nameri shisheto s rakiqta!");
						kubratHit *= 3;
					}

					if (isKubratSick) {
						kubratHit /= 2;
					}

					System.out.println("Kobrata go prasna sys sila " + kubratHit);
					klichkoHealth = (byte) ((klichkoHealth >= kubratHit) ? klichkoHealth - kubratHit : 0);

					System.out.println("Na Klichko mu ostana " + klichkoHealth + " kryv.");
				}
			}

			if (kubratHealth > 0) {
				System.out.println("Kubrat mu srita zadnika na onq piq ukrainec!");
				koiPecheli = 1;
			}

			if (klichkoHealth > 0) {
				System.out.println("Klichko mu srita zadnika na nashiq, no prodyljavame napred!");
				koiPecheli = 2;
			}

			resultati[simulacia - 1] = koiPecheli;
			System.out.println("*** End of simulation " + simulacia + " ***");
			System.out.println("***====***");
		}
		System.out.println("---------------");
		System.out.println("-- SUMMARY --");
		for (int index = 0; index < 12; index++) {
			if (resultati[index] == 0) {
				System.out.println("Kravata pecheli rund " + (index + 1));
			}
			if (resultati[index] == 1) {
				System.out.println("Kubrat pecheli rund " + (index + 1));
			}
			if (resultati[index] == 2) {
				System.out.println("Klichko pecheli rund " + (index + 1));
			}
			if (resultati[index] == 5) {
				System.out.println("Nikoi ne pecheli rund " + (index + 1));
			}
		}

	}

}
