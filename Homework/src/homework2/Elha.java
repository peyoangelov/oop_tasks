package homework2;

import java.util.Scanner;

public class Elha {

	public static void main(String[] args) {
		System.out.println("Vavedete visochina na triugalnika");
		Scanner sc1 = new Scanner(System.in);
		int n = sc1.nextInt();
		int spaces = n;
		int stars = 1;
		int broiTopki = (int) (0.2 * (n - 2));

		for (int row = 1; row <= n + 4; row++) {

			if (row == n + 2 || row == n + 3 || row == n + 4) {
				stars = 1;
				spaces = n;
			}
			for (int space = 0; space <= spaces; space++) {
				System.out.print(" ");

			}
			int poziciqNaTopka = (int) (Math.random() * 10);

			for (int star = 1; star <= stars; star++) {

				if ((row == 1 || row == n + 1) || (star == 1 || star == stars)) {

					System.out.print("*");
				} else {
					if (broiTopki >= 1 && star == poziciqNaTopka) {
						System.out.print("O");
						broiTopki--;
					} else
						System.out.print(" ");
				}
			}
			System.out.println();
			if (row != 1) {
				spaces--;
				stars += 2;
			}
		}
		System.out.println();
		spaces = n+1 ;
		stars = 1;
		broiTopki = (int) (0.2 * (n - 2));
		for (int row = n + 4; row >= 1; row--) {

			if (row == n + 2 || row == n + 3 || row == n + 4 || row == 1) {
				stars = 1;
				spaces = n +1;
			}
			if (row == n + 1) {
				stars = 1;
				for (int x = 1; x < n; x++) {
					stars = stars + 2;

				}
				spaces = 2;
			}
			for (int space = spaces; space >= 1; space--) {
				System.out.print(" ");

			}
			int poziciqNaTopka = (int) (Math.random() * 10);

			for (int star = stars; star >= 1; star--) {

				if ((row == 1 || row == n + 1) || (star == 1 || star == stars)) {

					System.out.print("*");
				} else {
					if (broiTopki >= 1 && star == poziciqNaTopka) {
						System.out.print("O");
						broiTopki--;
					} else
						System.out.print(" ");
				}
			}
			System.out.println();
			if (row != 1 || row == n + 2 || row == n + 3 || row == n + 4) {
				spaces++;
				stars -= 2;
			}
		}
	}

}
