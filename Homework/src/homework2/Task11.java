package homework2;

import java.util.Scanner;

public class Task11 {

	public static void main(String[] args) {
		System.out.println("Vavedete visochina na triugalnika");
		Scanner sc1 = new Scanner(System.in);
		int n = sc1.nextInt();
		int spaces = n - 1;
		int stars = 1;
		// ZAPALNEN triagalnik
		/*
		 * for (int row = 1; row <= n; row++) { for (int space = 0; space <=
		 * spaces; space++) { System.out.print(" "); } for (int star = 1; star
		 * <= stars; star++) { System.out.print("*"); }
		 * 
		 * System.out.println(); spaces--; stars += 2; }
		 */
		// NEZAPalnen triagalnik

		for (int row = 1; row <= n; row++) {
			for (int space = 0; space <= spaces; space++) {
				System.out.print(" ");
			}
			for (int star = 1; star <= stars; star++) {

				if ((row == 1 || row == n) || (star == 1 || star == stars)) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}

			System.out.println();
			spaces--;
			stars += 2;
		}
	}
}
