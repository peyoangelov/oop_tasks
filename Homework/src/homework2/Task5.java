package homework2;

import java.util.Scanner;

public class Task5 {

	public static void main(String[] args) {
		System.out.println("Enter number X");
		Scanner num1 = new Scanner(System.in);
		int x = num1.nextInt();
		System.out.println("Enter number Y");
		int y = num1.nextInt();
		int z;
		if (x > y) {
			z = y;
			while (x >= z) {
				System.out.println(z);
				z++;
			}
		}
		if (x < y) {
			z = x;
			while (z <= y) {
				System.out.println(z);
				z++;
			}
		}
		if (x == y) {
			System.out.println("Chislata sa ravni");
		}
	}
}
