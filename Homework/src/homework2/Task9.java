package homework2;

import java.util.Scanner;

public class Task9 {

	public static void main(String[] args) {
		System.out.println("Vavedete chislo A");
		Scanner sc1 = new Scanner(System.in);
		int a = sc1.nextInt();
		System.out.println("Vavedete chislo B");
		int b = sc1.nextInt();
		int sum = 0;
		if (a < b) {

			for (int x = a; (sum < 200) && (x < b); x++) {
				if (x % 3 == 0) {
					System.out.println("Skip" + x);
				} else {
					System.out.println(x * x);
					sum += (x * x);
				}

			}
		} else {
			System.out.println("Vavedete razlichni narastvashti chisla");
		}
	}

}
