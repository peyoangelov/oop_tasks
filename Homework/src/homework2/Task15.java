package homework2;

import java.util.Scanner;

public class Task15 {

	public static void main(String[] args) {
		System.out.println("Vavedete chislo");
		Scanner sc1 = new Scanner(System.in);
		int chislo = sc1.nextInt();
		int sum = 0;
		int start = 1;

		do {
			sum += start;
			start++;
		} while (start <= chislo);
		System.out.println(sum);
	}

}
