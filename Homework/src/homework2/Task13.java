package homework2;

import java.util.Scanner;

public class Task13 {

	public static void main(String[] args) {
		Scanner sc1 = new Scanner(System.in);
		int sum = sc1.nextInt();
		if (sum >= 2 && sum <= 27) {
			for (int x = 100; x <= 999; x++) {
				byte edinici = (byte) (x % 10);
				byte desetici = (byte) ((x / 10) % 10);
				byte stotici = (byte) (x / 100);
				if (edinici + desetici + stotici == sum) {
					System.out.println(x);
				}
			}
		} else {
			System.out.println("Vavedete chislo mejdu 2 i 27");
		}

	}

}
