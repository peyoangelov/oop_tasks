package homework2;

import java.util.Scanner;

import org.omg.CosNaming.NamingContextPackage.NotEmpty;

public class Task21 {

	public static void main(String[] args) {
		System.out.println("Vavedete chislo ot 1 do 51");
		Scanner sc = new Scanner(System.in);
		byte chislo = sc.nextByte();
		byte cvqtNaKarta;
		byte nomerNaKarta;
		if (chislo >= 1 && chislo <= 51) {
			cvqtNaKarta = (byte) (chislo % 4);
			nomerNaKarta = (byte) (chislo / 4);

			if (cvqtNaKarta != 0) {
				nomerNaKarta++;
			} else {
				cvqtNaKarta = 4;
			}
			for (byte x = nomerNaKarta; x <= 13; x++) {

				for (byte z = cvqtNaKarta; z <= 4; z++) {
					if (nomerNaKarta == 10) {
						System.out.print("Vale");
					} else if (x == 11) {
						System.out.print("Dama");
					} else if (x == 12) {
						System.out.print("Pop");
					} else if (x == 13) {
						System.out.print("Aso");
					} else {
						System.out.print(x + 1);

					}
					if (z == 1) {
						System.out.print(" Spatiq, ");
					}
					if (z == 2) {
						System.out.print(" Karo, ");
					}
					if (z == 3) {
						System.out.print(" Kupa, ");
					}
					if (z == 4) {
						if (x == 13) {
							System.out.println(" Pika");
						} else
							System.out.print(" Pika, ");
					}

				}
				cvqtNaKarta = 1;
			}

		} else {
			System.out.println("Vavedete chislo ot 1 do 51");
		}

	}

}
