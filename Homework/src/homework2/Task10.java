package homework2;

import java.util.Scanner;

public class Task10 {

	public static void main(String[] args) {
		System.out.println("Vavedete chislo za proverka:");
		Scanner scan = new Scanner(System.in);

		int num = scan.nextInt();
		int temp;
		boolean prostoLiE = true;
		for (int i = 2; i <= num / 2; i++) {
			temp = num % i;
			if (temp == 0) {
				prostoLiE = false;
				break;
			}
		}

		if (prostoLiE)
			System.out.println(num + " e prosto chislo");
		else
			System.out.println(num + " NE E prosto chislo");
	}

}
