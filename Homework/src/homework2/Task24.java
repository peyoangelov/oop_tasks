package homework2;

import java.util.Scanner;

public class Task24 {

	public static void main(String[] args) {
		System.out.println("Vavedete chislo ot 10 do 30000");
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();

		if (number <= 30000 && number >= 10) {
			int temp, reverse = 0, reminder;
			temp = number;
			do {
				reminder = temp % 10;
				reverse = reverse * 10 + reminder;
				temp = temp / 10;
			} 
			while (temp > 0);
			System.out.println("Obarnatoto chislo e " + reverse);
			if (number == reverse) {
				System.out.println("chislotot e palindrom");
			} else {
				System.out.println("chisloto NE E palindrom");
			}

		} else {
			System.out.println("Vavedete chislo ot 10 do 30000");
		}

	}

}
