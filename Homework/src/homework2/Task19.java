package homework2;

import java.util.Scanner;

public class Task19 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Vavedete chislo ot 10 do 99");
		int chislo = sc.nextInt();
		if (chislo <= 99 && chislo >= 10) {
			int temp;
			do {
				if (chislo % 2 == 0) {
					temp = (int) (chislo * 0.5);
				} else {
					temp = chislo * 3 + 1;
				}
				System.out.print(temp+" ");
				chislo = temp;
			} while (chislo > 1);

		} else {
			System.out.println("Vavedete chislo ot 10 do 99");
		}

	}

}
