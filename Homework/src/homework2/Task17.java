package homework2;

import java.util.Scanner;

public class Task17 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int strana = sc.nextInt();
		char znak = sc.next().charAt(0);
		if (strana <= 20 && strana >= 3) {
			for (int j = 0; j < strana; j++) {

				for (int i = 0; i < strana; i++) {
					if (j == 0 || j == strana - 1) {
						System.out.print("*");
					} else {
						if (i == 0 || i == strana - 1) {
							System.out.print("*");

						} else {
							System.out.print(znak);
						}
					}

				}
				System.out.println();

			}

		}

	}

}
