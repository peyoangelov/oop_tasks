package homework2;

import java.util.Scanner;

public class Task14 {

	public static void main(String[] args) {
		Scanner sc1 = new Scanner(System.in);
		int chisloN = sc1.nextInt();

		if (chisloN >= 10 && chisloN <= 200) {
			for (int x = chisloN - 1; x >= 1; x--) {
				if (x % 7 == 0) {
					System.out.println(x);
				}
			}
		} else {
			System.out.println("Vavedete chislo mejdu 10 i 200");
		}

	}

}
