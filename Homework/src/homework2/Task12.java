package homework2;

public class Task12 {

	public static void main(String[] args) {
		for (int x = 100; x <= 999; x++) {
			byte edinici = (byte) (x % 10);
			byte desetici = (byte) ((x / 10) % 10);
			byte stotici = (byte) (x / 100);
			if ((edinici != desetici) && (desetici != stotici) && (stotici != edinici)) {
				System.out.println(x);
			}
		}

	}

}
