package homework3;

import java.util.Scanner;

public class Task8 {

	public static void main(String[] args) {
		System.out.println("Vavedete daljina na masiwa");
		Scanner sc = new Scanner(System.in);
		int daljina = sc.nextInt();
		int[] array = new int[daljina];
		System.out.println("vavedete elementite na masiva");
		for (int index = 0; index < daljina; index++) {
			array[index] = sc.nextInt();
		}
		int kolkoPatiSeSreshtat = 1;
		int element = 0;
		for (int index = 0; index < daljina - 1; index++) {
			if (array[index] == array[index + 1]) {
				if (element == array[index]) {
					kolkoPatiSeSreshtat++;
				} else {
					element = array[index];
					kolkoPatiSeSreshtat = 2;
				}
			}
		}
		if (kolkoPatiSeSreshtat == 1) {
			System.out.println("Nqma posledowatelno powtarqshti se chisla");
		} else {
			System.out.print("Maksimalnata redica e ");
			for (int index = 0; index < kolkoPatiSeSreshtat; index++) {
				System.out.print(element + " ");

			}
		}
	}

}
