package homework3;

import java.util.Scanner;

public class Task17 {

	public static void main(String[] args) {
		System.out.println("Vavedete daljina na masiva");
		Scanner sc = new Scanner(System.in);
		int[] masiv = new int[sc.nextInt()];
		boolean isZigzag = false;
		System.out.println("Vavedete elementite na masiva");
		for (int index = 0; index < masiv.length; index++) {
			masiv[index] = sc.nextInt();
		}
		for (int index = 1; index < masiv.length - 1; index += 2) {
			if ((masiv[index] < index + 1) && (masiv[index] < index - 1)
					|| (masiv[index] > masiv[index + 1]) && masiv[index] > masiv[index] - 1) {
				isZigzag = true;
			} else {
				isZigzag = false;
				break;
			}
		}
		if (isZigzag == true) {
			System.out.println("Izpalnqwa iziskwaniqta");
		} else {
			System.out.println("Ne izpalnqwa iziskwaniqta");
		}
	}

}
