package homework3;

import java.util.Arrays;
import java.util.Scanner;

public class Task9 {

	public static void main(String[] args) {
		System.out.println("Vavedete daljina na masiva");
		Scanner sc = new Scanner(System.in);
		int daljinaNaMasiva = sc.nextInt();
		int[] masiv = new int[daljinaNaMasiva];
		int[] dopalnitelenMasiv = new int[daljinaNaMasiva];
		System.out.println("Vavedete elementite na masiva");
		if (daljinaNaMasiva > 0 && daljinaNaMasiva < 91999) {
			for (int index = 0; index < daljinaNaMasiva; index++) {
				masiv[index] = sc.nextInt();
			}
			for (int index = daljinaNaMasiva - 1; index >= 0; index--) {
				dopalnitelenMasiv[daljinaNaMasiva - 1 - index] = masiv[index];
			}
			for (int index = 0; index < masiv.length / 2; index++) {
				int temp = masiv[index];
				masiv[index] = masiv[daljinaNaMasiva - 1 - index];
				masiv[daljinaNaMasiva - 1 - index] = temp;
			}

			for (int index = 0; index < daljinaNaMasiva; index++) {
				System.out.print(masiv[index] + " ");

			}
			System.out.println();
			for (int index = 0; index < daljinaNaMasiva; index++) {
				System.out.print(dopalnitelenMasiv[index] + " ");
			}

		} else {
			System.out.println("Vavedete daljina na masiva po-golqma ot 0");
		}
	}

}
