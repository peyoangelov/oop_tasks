package homework3;

import java.util.Scanner;

public class TaskFromLecture {

	public static void main(String[] args) {
		final int NUMBER_OF_WORKERS = 5;
		String[] names = new String[NUMBER_OF_WORKERS];
		int[] salaries = new int[NUMBER_OF_WORKERS];
		System.out.println("Vavedete ime na rabotnik i zaplatata mu:");
		Scanner sc = new Scanner(System.in);
		for (int index = 0; index < names.length; index++) {
			names[index] = sc.next();
			salaries[index] = sc.nextInt();
		}

		for (int index = 0; index < names.length; index++) {
			System.out.println(names[index]);
		}

		for (int index = names.length - 1; index >= 0; index--) {
			System.out.println(names[index]);
		}
		int maxSalary = salaries[0];
		int indexOfMaxSalary = 0;

		for (int index = 0; index < salaries.length; index++) {
			if (salaries[index] > maxSalary) {
				maxSalary = salaries[index];
				indexOfMaxSalary = index;
			}
		}

		System.out.println("Rabotnikut koito poluchava nai-mnogo e " + names[indexOfMaxSalary] + " : "
				+ salaries[indexOfMaxSalary]);

		int minSalary = salaries[0];
		int indexOfMinSalary = 0;

		for (int index = 0; index < salaries.length; index++) {
			if (salaries[index] < minSalary) {
				minSalary = salaries[index];
				indexOfMinSalary = index;
			}
		}

		System.out.println("Rabotnikut koito poluchava nai-malko e " + names[indexOfMinSalary] + " : "
				+ salaries[indexOfMinSalary]);

		long sum = 0;
		for (int index = 0; index < salaries.length; index++) {
			sum += salaries[index];
		}
		System.out.println("Sumata ot zaplatite e " + sum);

		System.out.println("Srednoaritmetichnoto e " + (sum / salaries.length));
	}

}
