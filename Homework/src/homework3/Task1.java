package homework3;

import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		System.out.print("Vavedete daljina na masiva ");
		Scanner sc = new Scanner(System.in);
		int broiNaElementite = sc.nextInt();
		int[] array = new int[broiNaElementite];
		System.out.print("Vavedete elementite ");
		for (int index = 0; index < broiNaElementite; index++) {
			array[index] = sc.nextInt();
		}
		int min = 31999;
		boolean isNum = false;
		for (int index = 0; index < broiNaElementite; index++) {
			if (array[index] % 3 == 0) {
				isNum = true;
				if (min > array[index]) {
					min = array[index];
				}
			}
  	}

		if (isNum) {
			System.out.println("Nai-malkoto chislo kratno na 3 e " + min);
		} else {
			System.out.println("Nqma takova chislo");
		}
	}
}
