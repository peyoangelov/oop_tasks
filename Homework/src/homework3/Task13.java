package homework3;

import java.util.Scanner;

public class Task13 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Vavedete chislo ");
		int number = sc.nextInt();
		int[] array = new int[32];
		int broqch = 0;

		for (int index = 0; number > 0; index++) {
			array[index] = number % 2;
			number = number / 2;
			broqch++;
		}
		for (int index = broqch - 1; index >= 0; index--) {
			System.out.print(array[index] + " ");
		}

	}

}
