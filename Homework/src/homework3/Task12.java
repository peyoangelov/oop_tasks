package homework3;

import java.util.Scanner;

public class Task12 {

	public static void main(String[] args) {
		int[] array = new int[7];
		Scanner sc = new Scanner(System.in);

		for (int index = 0; index < array.length; index++) {
			array[index] = sc.nextInt();
		}
		int temp = array[0];
		array[0] = array[1];
		array[1] = temp;

		array[2] += array[3];
		array[3] = array[2] - array[3];
		array[2] -= array[3];

		array[4] *= array[5];
		array[5] = array[4] / array[5];
		array[4] /= array[5];

		for (int index = 0; index < array.length; index++) {
			System.out.print(array[index] + " ");
		}

	}

}
