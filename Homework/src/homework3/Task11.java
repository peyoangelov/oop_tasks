package homework3;

import java.util.Scanner;

public class Task11 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int[] array = new int[7];
		int count = 0;
		System.out.println("Vavedete elementite na masiva");
		for (int index = 0; index < array.length; index++) {
			array[index] = sc.nextInt();
		}
		for (int index = 0; index < array.length; index++) {
			if ((array[index] % 5 == 0) && (array[index] > 5)) {

				count++;

				if (count == 1) {

					System.out.print(array[index]);
				} else {
					System.out.print(", " + array[index]);
				}
			}
		}
		System.out.println(" " + count + " chisla");
	}

}
