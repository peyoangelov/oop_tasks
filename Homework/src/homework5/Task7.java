package homework5;

import java.util.Scanner;

public class Task7 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Vaveddete dumi razdeleni s interval");
		String vhod = sc.nextLine();
		String[] dumi = vhod.split(" ");
		String naiDalgataDuma = "";

		for (int index = 0; index < dumi.length; index++) {
			if (dumi[index].length() > naiDalgataDuma.length()) {
				naiDalgataDuma = dumi[index];

			}
		}
		System.out.println(dumi.length + " dumi " + " Nai-dalgata duma e " + naiDalgataDuma + " s daljina "
				+ naiDalgataDuma.length() + " simvola");

	}

}
