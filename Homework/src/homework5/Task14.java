package homework5;

import java.util.Scanner;

public class Task14 {
	public static int factorial(int n) {
		int fact = 1;
		for (int i = 1; i <= n; i++) {
			fact *= i;
		}
		return fact;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int chislo = sc.nextInt();
		int factorial = factorial(chislo);
		System.out.println(factorial);

	}

}
