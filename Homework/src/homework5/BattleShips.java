package homework5;

public class BattleShips {

	public static void main(String[] args) {
		char[][] sea = new char[22][30];
		int chance = 0; // za Math.random
		int iterationsNumber = 0;
		boolean isZvezdaCrossBorder = false;
		boolean isKliombaCrossBorder = false;
		for (int row = 0; row < sea.length; row++) {
			if (row == 0 || row == sea.length - 1) {
				for (int col = 0; col < sea[0].length; col++) {
					sea[row][col] = '-';
				}
			} else {
				chance = (int) (Math.random() * 10 + 2) / 2;// ot 1 do 5 vkl za
															// vseki red
				for (int index = 0; index < chance; index++) {
					sea[row][index] = '*';
				}
				chance = (int) (Math.random() * 10 + 2) / 2;
				for (int index = sea[0].length - 1; index > sea[0].length - 1 - chance; index--) {
					sea[row][index] = '@';
				}

			}
		}

		for (int row = 0; row < sea.length; row++) {
			for (int col = 0; col < sea[0].length; col++) {
				System.out.print(sea[row][col]);
			}
			System.out.println();
		}
		System.out.println();
		do {
			for (int row = 1; row < sea.length - 1; row++) {
				int col = 0;
				int zvezdi = 0;
				int kliombi = 0;
				boolean isThereZvezdi = false;
				while (sea[row][col] == '\u0000') {
					col++;
				}
				while (sea[row][col] == '*') {
					zvezdi++;
					isThereZvezdi = true;
					col++;
				}
				if (!isThereZvezdi) {
					int indexKiomba = col;
					while (sea[row][col] == '@') {
						kliombi++;
						col++;
					}
					if (indexKiomba == 0) {
						isKliombaCrossBorder = true;
					} else {
						sea[row][indexKiomba - 1] = '@';
						sea[row][indexKiomba + kliombi - 1] = '\u0000';
					}
				} else {
					int indexZvezda = col;
					if (sea[row][indexZvezda] == '@') {

						kliombi = sea[0].length - indexZvezda - iterationsNumber;
						if (zvezdi > kliombi) {
							for (int index = 0; index < kliombi; index++) {
								sea[row][indexZvezda + index] = '\u0000';
							}
							while (sea[row][col] == '*') {
								zvezdi++;
								col++;

							}
						}
						if (zvezdi < kliombi) {
							for (int index = 0; index < zvezdi; index++) {
								sea[row][indexZvezda - zvezdi + index] = '\u0000';
							}
						}
						if (zvezdi == kliombi) {
							if (Math.random() < 0.5) {
								for (int index = 0; index < kliombi; index++) {
									sea[row][indexZvezda + index] = '\u0000';
								}
							} else {
								for (int index = 0; index < zvezdi; index++) {
									sea[row][indexZvezda - 1 - zvezdi + index] = '\u0000';
								}
							}
						}
					} else {
						while (sea[row][col] == '\u0000' && col < sea[0].length - 1) {
							col++;
						}
						if (col == sea[0].length - 1 && sea[row][col] != '@') {
							if (indexZvezda + 1 <= sea[0].length - 1) {
								sea[row][indexZvezda] = '*';
								sea[row][indexZvezda - zvezdi] = '\u0000';
							} else {
								sea[row][indexZvezda] = '*';
								sea[row][indexZvezda - zvezdi] = '\u0000';
								isZvezdaCrossBorder = true;
							}
						} else {
							int indexKiomba = col;
							kliombi = sea[0].length - indexKiomba - iterationsNumber;
							sea[row][indexZvezda] = '*';
							sea[row][indexZvezda - zvezdi] = '\u0000';
							sea[row][indexKiomba - 1] = '@';
							sea[row][indexKiomba + kliombi - 1] = '\u0000';
						}
					}
				}
			}
			iterationsNumber++;
			for (int row = 0; row < sea.length; row++) {
				for (int col = 0; col < sea[0].length; col++) {
					System.out.print(sea[row][col]);
				}
				System.out.println();
			}
			System.out.println();
		} while (!isZvezdaCrossBorder && !isKliombaCrossBorder);
		if (isKliombaCrossBorder) {
			System.out.println("!!! KLIOMBA WON !!!");
		} else {
			System.out.println("!!! ZVEZDA WON !!!");
		}

	}

}
