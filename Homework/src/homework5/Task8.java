package homework5;

import java.util.Scanner;

public class Task8 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String vhod = sc.nextLine();
		StringBuilder dumataNaObratno = new StringBuilder();

		for (int index = vhod.length() - 1; index >= 0; index--) {
			dumataNaObratno.append(vhod.charAt(index));
		}
		if (dumataNaObratno.toString().equals(vhod)) {
			System.out.println("Palindrom e");
		} else {
			System.out.println("Ne e palindrom");
		}

	}

}
