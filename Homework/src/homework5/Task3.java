package homework5;

import java.util.Scanner;

public class Task3 {

	public static void main(String[] args) {
		System.out.println("Vavedete dve dumi sys zapetaq mejdu tqh");
		Scanner sc = new Scanner(System.in);
		String vhod = sc.nextLine();
		boolean isWordsEqual = true;
		if (vhod.contains(",")) {
			String[] dumi = vhod.split(",");
			if (dumi[0].length() == dumi[1].length()) {
				System.out.println("Dumite sa s ednakwa duljina");
				for (int index = 0; index < dumi[0].length(); index++) {
					if (dumi[0].charAt(index) != dumi[1].charAt(index)) {
						System.out.println(index + 1 + " " + dumi[0].charAt(index) + " - " + dumi[1].charAt(index));
						isWordsEqual = false;
					}
				}
				if (isWordsEqual) {
					System.out.println("Dumite sa ednakwi");
				}
			}
			if (dumi[0].length() > dumi[1].length()) {
				System.out.println("Dumite sa s razlichna duljina");
				if (dumi[1].equals(dumi[0].substring(0, dumi[1].length()))) {
					System.out.println("Nqma razlika w poziciqta");
				} else {
					for (int index = 0; index < dumi[1].length(); index++) {
						if (dumi[0].charAt(index) != dumi[1].charAt(index)) {
							System.out.println(index + 1 + " " + dumi[0].charAt(index) + "-" + dumi[1].charAt(index));
						}
					}
				}

			}
			if (dumi[0].length() < dumi[1].length()) {
				System.out.println("Dumite sa s razlichna duljina");
				if (dumi[0].equals(dumi[1].substring(0, dumi[0].length()))) {
					System.out.println("Nqma razlika w poziciqta");

				} else {
					for (int index = 0; index < dumi[0].length(); index++) {
						if (dumi[0].charAt(index) != dumi[1].charAt(index)) {
							System.out.println(index + 1 + " " + dumi[0].charAt(index) + "-" + dumi[1].charAt(index));
						}
					}
				}

			}

		} else {
			System.out.println("Vavedete dve dumi sys zapetaq mejdu tqh");
		}
	}

}
