package homework5;

import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Vavedete dve dumi s daljina ot 6 do 20 znaka");
		StringBuilder duma1 = new StringBuilder(sc.nextLine());
		StringBuilder duma2 = new StringBuilder(sc.nextLine());

		if (duma1.length() > 6 && duma1.length() < 20 && duma2.length() > 6 && duma2.length() < 20) {
			String firstFive1 = duma1.substring(0, 5);
			String firstFive2 = duma2.substring(0, 5);
			duma1.replace(0, 5, firstFive2);
			duma2.replace(0, 5, firstFive1);
			if (duma1.length() > duma2.length()) {
				System.out.println(duma1.length() + " " + duma1);
			} else {
				System.out.println(duma2.length() + " " + duma2);
			}

		} else {
			System.out.println("Vavedete dve dumi s daljina ot 6" + " do 20 znaka");
		}

	}

}
