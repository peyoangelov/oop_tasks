package homework5;

import java.util.Scanner;

public class LecturesTask1 {
	public static void printarray(int[]array){
		for(int index=0;index<array.length;index++){
			System.out.print(array[index]);
		}
	}

	public static void main(String[] args) {
		int[] array= new int[9];
		Scanner sc= new Scanner(System.in);
		for( int index=0;index<array.length;index++){
			array[index] = sc.nextInt();
		}
		printarray(array);
		
	}

}
