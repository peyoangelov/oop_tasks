package homework5;

import java.util.Scanner;

public class Task12 {
	public static int[] fillArray(int number) {
		int[] array = new int[number];
		for (int index = 0; index < array.length; index++) {
			array[index] = index + 1;
		}
		return array;

	}

	public static void main(String[] args) {
		System.out.println("Vavedete chislo");
		Scanner sc = new Scanner(System.in);
		int chislo = sc.nextInt();
		int[] array = fillArray(chislo);
		for (int index = 0; index < array.length; index++) {
			System.out.print(array[index]+" ");
		}

	}

}
