package homework5;

import java.util.Scanner;

public class Task9 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Vavedete dumi,bukvi,chisla");
		String vhod = sc.nextLine();
		int suma = 0;
		StringBuilder chislo = new StringBuilder();
		for (int index = 0; index < vhod.length(); index++) {
			if (vhod.charAt(index) == '-' && index + 1 < vhod.length() && vhod.charAt(index + 1) >= 48
					&& vhod.charAt(index + 1) <= 57) {
				do {
					chislo.append(vhod.charAt(index));
					index++;
				} while (index < vhod.length() && (vhod.charAt(index) >= 48 && vhod.charAt(index) <= 57));
			}
			if (index >= vhod.length()) {
				suma += Integer.parseInt(chislo.toString());

				break;
			}
			if (vhod.charAt(index) >= 48 && vhod.charAt(index) <= 57) {
				do {
					chislo.append(vhod.charAt(index));
					index++;
				} while (index < vhod.length() && (vhod.charAt(index) >= 48 && vhod.charAt(index) <= 57));
			}

			if (!chislo.toString().equals("")) {
				suma += Integer.parseInt(chislo.toString());
				chislo = new StringBuilder();	
				index--;
			}
		}

		System.out.println("Sumata e " + suma);

	}

}
