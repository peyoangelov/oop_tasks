package homework5;

public class Task13 {
	public static int[] joinArrays(int[] array1, int[] array2) {
		int[] array3 = new int[array1.length + array2.length];
		for (int index = 0; index < array1.length; index++) {
			array3[index] = array1[index];
		}
		for (int index = 0; index < array2.length; index++) {
			array3[(array1.length) + index] = array2[index];
		}
		return array3;
	}

	public static void main(String[] args) {
		int[] array1 = { 1, 2, 3, 4, 5 };
		int[] array2 = { 9, 8, 7, 6, };
		int[] array3 = joinArrays(array1, array2);
		for (int index = 0; index < array3.length; index++){
			System.out.print(array3[index]+" ");
		}
	}

}
