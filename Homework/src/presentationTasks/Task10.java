package presentationTasks;

public class Task10 {

	public static void main(String[] args) {
		int[] array = { 4, 6, 4, 1, 4, 4, 3, 4, 5, 6, 7 };
		int kolkoChestoSeSreshta = 0;
		int maxSreshtanElement = 0;
		int elemantaE = array[0];

		for (int index = 0; index < array.length; index++) {
			kolkoChestoSeSreshta = 0;
			for (int indexy = 0; indexy < array.length; indexy++) {
				if (array[index] == array[indexy]) {
					kolkoChestoSeSreshta++;
				}
			}
			if (kolkoChestoSeSreshta > maxSreshtanElement) {
				elemantaE = array[index];
				maxSreshtanElement = kolkoChestoSeSreshta;

			}
		}
		System.out.println(elemantaE + " (" + maxSreshtanElement + " times)");

	}

}
