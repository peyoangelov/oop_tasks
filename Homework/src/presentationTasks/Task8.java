package presentationTasks;

import java.util.Scanner;

public class Task8 {

	public static void main(String[] args) {
		boolean[][] array = new boolean[4][4];
		boolean IsTrue = false;
		Scanner sc = new Scanner(System.in);

		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				System.out.println("Vavedete true ili false za red: " + row + " i kolona: " + col);
				array[row][col] = sc.nextBoolean();
			}
		}
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				if (row + col < array[0].length - 1 && array[row][col]) {
					System.out.println("Ima true");
					IsTrue = true;
					break;
				}
			}
		}
		if (!IsTrue) {
			System.out.println("Nqma true elementi!");
		}

	}

}
