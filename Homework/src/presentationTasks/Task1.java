package presentationTasks;

import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		int[] array = new int[8];
		Scanner sc = new Scanner(System.in);
		boolean otricatelniLiSa = false;
		System.out.println("Vavedete elementite na masiva");
		for (int index = 0; index < array.length; index++) {
			array[index] = sc.nextInt();
		}
		for (int index = 0; index < array.length; index++) {
			if (array[index] < 0) {
				otricatelniLiSa = true;
				break;
			}
		}
		if (otricatelniLiSa) {
			System.out.println("Ima otricatelno chislo");
		} else {
			System.out.println("Vsichki chisla sa polojitelni");
		}

	}

}
