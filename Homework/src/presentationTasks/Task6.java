package presentationTasks;

import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		char[][] array = new char[4][4];
		Scanner sc = new Scanner(System.in);
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				System.out.println("Vavedete elementite na masiva za red " + row + " i kolona " + row);
				array[row][col] = sc.nextLine().charAt(0);
			}
		}
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				if (row == col) {
					System.out.println(array[row][col]);
				}
			}
		}

	}

}
