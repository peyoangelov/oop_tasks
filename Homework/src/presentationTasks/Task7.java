package presentationTasks;

import java.util.Scanner;

public class Task7 {

	public static void main(String[] args) {
		int[][] array = new int[4][4];
		int proizvedenie = 1;
		Scanner sc = new Scanner(System.in);
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				System.out.println("Vavedete elementite za red " + row + " i kolona " + col);
				array[row][col] = sc.nextInt();
			}
		}

		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				if (row > col) {
					proizvedenie *= array[row][col];

				}
			}
		}
		System.out.println("Proizvedenieto na elementite pod glavniq diagonal e " + proizvedenie);
	}
}
