package presentationTasks;

import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		int[] array = new int[8];
		boolean nazabenLiE = true;
		Scanner sc = new Scanner(System.in);
		System.out.println("Vavedete elementite na masiva ");
		for (int index = 0; index < array.length; index++) {
			array[index] = sc.nextInt();
		}
		for (int index = 1; index < array.length - 1; index += 2) {
			if (!(array[index] > array[index - 1] && array[index] < array[index + 1])) {
				nazabenLiE = false;
				break;
			}
		}
		if (nazabenLiE) {
			System.out.println("Masivat e nazaben");
		} else {
			System.out.println("Ne e nazaben");
		}

	}

}
