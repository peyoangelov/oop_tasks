package presentationTasks;

import java.util.Scanner;

public class EncriptDecript {
	public static void fillArray(char[][] tablica) {
		char[] azbuka = new char[25];
		for (int index = 0; index < azbuka.length; index++) {
			azbuka[index] = (char) (65 + index);
		}
		for (int row = 0; row < tablica.length; row++) {
			for (int col = 0; col < tablica[0].length; col++) {
				int nomerNabukva = (int) (Math.random() * 25);
				while (tablica[row][col] == '\u0000') {
					if (azbuka[nomerNabukva] == '*') {
						nomerNabukva = (int) (Math.random() * 25);
					} else {
						tablica[row][col] = azbuka[nomerNabukva];
						azbuka[nomerNabukva] = '*';
					}
				}
			}
		}
		for (int row = 0; row < tablica.length; row++) {
			for (int col = 0; col < tablica[0].length; col++) {
				System.out.print(tablica[row][col] + " ");
			}
			System.out.println();
		}
	}

	public static String encript(char[][] table, String vhod) {
		StringBuilder code = new StringBuilder();
		for (int index = 0; index < vhod.length(); index += 2) {
			char bukva1 = vhod.charAt(index);
			char bukva2 = vhod.charAt(index + 1);
			int[] coordBukva1 = findCoordinate(table, bukva1);
			int[] coordBukva2 = findCoordinate(table, bukva2);

			if (coordBukva1[0] == coordBukva2[0]) {
				if (coordBukva1[1] + 1 >= table[0].length) {
					code.append(table[coordBukva1[0]][0]);
				} else {
					code.append(table[coordBukva1[0]][coordBukva1[1] + 1]);
				}
				if (coordBukva2[1] + 1 >= table[0].length) {
					code.append(table[coordBukva2[0]][0]);
				} else {
					code.append(table[coordBukva2[0]][coordBukva2[1] + 1]);
				}

			} else if (coordBukva1[1] == coordBukva2[1]) {
				if (coordBukva1[0] + 1 >= table.length) {
					code.append(table[0][coordBukva1[1]]);
				} else {
					code.append(table[coordBukva1[0] + 1][coordBukva1[1]]);
				}
				if (coordBukva2[0] + 1 >= table.length) {
					code.append(table[0][coordBukva2[1]]);
				} else {
					code.append(table[coordBukva2[0] + 1][coordBukva2[1]]);
				}
			} else {
				code.append(table[coordBukva1[0]][coordBukva2[1]]);
				code.append(table[coordBukva2[0]][coordBukva1[1]]);
			}

		}
		return (code.toString());
	}

	public static String decript(char[][] table, String vhod) {
		StringBuilder code = new StringBuilder();
		for (int index = 0; index < vhod.length(); index += 2) {
			char bukva1 = vhod.charAt(index);
			char bukva2 = vhod.charAt(index + 1);
			int[] coordBukva1 = findCoordinate(table, bukva1);
			int[] coordBukva2 = findCoordinate(table, bukva2);

			if (coordBukva1[0] == coordBukva2[0]) {
				if (coordBukva1[1] == 0) {
					code.append(table[coordBukva1[0]][table[0].length - 1]);
				} else {
					code.append(table[coordBukva1[0]][coordBukva1[1] - 1]);
				}
				if (coordBukva2[1] == 0) {
					code.append(table[coordBukva2[0]][table[0].length - 1]);
				} else {
					code.append(table[coordBukva2[0]][coordBukva2[1] - 1]);
				}

			} else if (coordBukva1[1] == coordBukva2[1]) {
				if (coordBukva1[0] == 0) {
					code.append(table[table.length - 1][coordBukva1[1]]);
				} else {
					code.append(table[coordBukva1[0] - 1][coordBukva1[1]]);
				}
				if (coordBukva2[0] == 0) {
					code.append(table[table.length - 1][coordBukva2[1]]);
				} else {
					code.append(table[coordBukva2[0] - 1][coordBukva2[1]]);
				}
			} else {
				code.append(table[coordBukva1[0]][coordBukva2[1]]);
				code.append(table[coordBukva2[0]][coordBukva1[1]]);
			}

		}
		return (code.toString());
	}

	public static int[] findCoordinate(char[][] tekst, char simbol) {
		int[] coordinates = new int[2];
		// System.out.println(tekst[0].length);

		for (int row = 0; row < tekst.length; row++) {
			for (int col = 0; col < tekst[0].length; col++) {
				if (tekst[row][col] == simbol) {
					coordinates[0] = row;
					coordinates[1] = col;
					break;
				}
				// break;
			}
		}
		return coordinates;
	}

	public static void main(String[] args) {
		char[][] tablica = new char[5][5];
		fillArray(tablica);
		Scanner sc = new Scanner(System.in);
		String vhod = sc.nextLine().toUpperCase();
		StringBuilder result = new StringBuilder();
		for (int index = 0; index < vhod.length(); index++) {
			if (vhod.charAt(index) >= 65 && vhod.charAt(index) <= 89) {
				result.append(vhod.charAt(index));
			}
		}
		if (result.length() % 2 != 0) {
			result.append('A');
		}
		
		String code = encript(tablica, result.toString());
		System.out.println(code);
		String dekriptiranataDumaE =decript(tablica, code);

		if (vhod.length()%2 !=0){
			System.out.println(dekriptiranataDumaE.substring(0, dekriptiranataDumaE.length()-1));
		}else {
			System.out.println(dekriptiranataDumaE);
		}
	}
}
