package presentationTasks;

import java.util.Scanner;

public class Task5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Vavedete kolko reda i  koloni na ima masivat");

		int m = sc.nextInt();
		int n = sc.nextInt();
		int[][] array = new int[m][n];
		int sumaNaElementiteNaReda = 0;
		int maxSuma = 0;
		int maxIndex = 0;
		System.out.println("Vavedete elementite na masiva");
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				System.out.println("Vavedete element za red:  " + row + " i kolona " + col);
				array[row][col] = sc.nextInt();
			}
		}
		for (int row = 0; row < array.length; row++) {
			for (int col = 0; col < array[0].length; col++) {
				sumaNaElementiteNaReda += array[row][col];
				if (sumaNaElementiteNaReda > maxSuma) {
					maxSuma = sumaNaElementiteNaReda;
					maxIndex = row;
				}
			}
		}
		System.out.println("Suma na Elementite na reda: " + sumaNaElementiteNaReda);
		System.out.println("Red: " + maxIndex + " e s nai-golqma suma na elementite");

	}

}
