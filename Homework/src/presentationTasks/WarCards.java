package presentationTasks;

import java.util.Arrays;

public class WarCards {
	public static void moveCards(String[] array) {
		for (int index = 1; !array[index].equals(null); index++) {
			array[index - 1] = array[index];
		}
	}

	public static void givingValueofCards(int card) {
		int carta = card / 10;
		int suit = card % 10;
		if (carta == 11) {
			System.out.println("Jack ");
		} else if (carta == 12) {
			System.out.println("Queen ");
		} else if (carta == 13) {
			System.out.println("King ");
		} else if (carta == 14) {
			System.out.println("Ace ");
		} else {
			System.out.println(carta);
		}
		if (suit == 1) {
			System.out.print(" Spades");
		} else if (suit == 2) {
			System.out.print(" Hearts");
		} else if (suit == 3) {
			System.out.print(" Diamonds");
		} else if (suit == 4) {
			System.out.print(" Clubs");
		}

	}

	public static int findingFreeSpace(String[] karti) {
		int index = 0;
		for (int indexK = 0; indexK < karti.length; indexK++) {
			if (karti[indexK].equals("")) {
				return index;

			}
		}
		return 0;
	}

	public static void main(String[] args) {
		int[] suits = { 1, 2, 3, 4 };
		// 1 Spades; 2 Hearts; 3 Diamonds; 4 Clubs
		int[] ranks = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
		// 11 Jack; 12 Queen ; 13 King; 14 Ace
		String[] deck = new String[52];
		String[] player1 = new String[52];
		String[] player2 = new String[52];
		boolean isItTheEnd = false;

		for (int index = 0; index < suits.length; index++) {
			for (int indexj = 0; indexj < ranks.length; indexj++) {
				deck[13 * index + indexj] = ranks[indexj] + "" + suits[index];
			} // palnim testeto, (ako ne go *s 13 ,shte varti samo ot0do13
		}

		for (int index = 0; index < player1.length / 2; index++) { // for dokato
																	// ne
																	// razdelim
																	// testeto
																	// na p1ip2
			int number = 0 + (int) (Math.random() * ((51 - 0) + 1));
			while (deck[number].equals("")) {
				number = 0 + (int) (Math.random() * ((51 - 0) + 1));
			}
			player1[index] = deck[number];
			deck[number] = ""; // mahame kartata ot decka koqto sme razdali

			number = 0 + (int) (Math.random() * ((51 - 0) + 1));
			while (deck[number].equals("")) {
				number = 0 + (int) (Math.random() * ((51 - 0) + 1));
			}
			player2[index] = deck[number]; // sashtoto za p2
			deck[number] = "";
		}

		// System.out.println (Arrays.toString(player1));
		while (player1[player1.length - 1] == null || player2[player2.length - 1] == null) {
			System.out.println("Igrach 1 dava");
			givingValueofCards(Integer.parseInt(player1[0]));
			System.out.println("Igrach 2 dava");
			givingValueofCards(Integer.parseInt(player2[0]));
			String card1 = player1[0];
			String card2 = player2[0];

			moveCards(player1);
			moveCards(player2);
			if (Integer.parseInt(player1[0]) / 10 > Integer.parseInt(player2[0]) / 10) {
				int freeSpace = findingFreeSpace(player1);
				player1[freeSpace] = card1;
				player1[freeSpace + 1] = card2;
			}
			if (Integer.parseInt(player1[0]) / 10 < Integer.parseInt(player2[0]) / 10) {
				int freeSpace = findingFreeSpace(player2);
				player2[freeSpace] = card1;
				player2[freeSpace + 1] = card2;
			} else {

			}
		}

	}

}
