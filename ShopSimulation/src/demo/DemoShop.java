package demo;

import enums.ProductEntity;
import enums.ProductType;
import interfaces.ICustomer;
import shopTask.Customer;
import shopTask.Product;
import shopTask.Shop;

public class DemoShop {
	public static void main(String[] args) {
		Shop shop = new Shop(6, "Billa", "Musagenica");
		Product meat = new Product(ProductEntity.BY_KILO, ProductType.MEAT, 2.5f, 35);
		Product fish = new Product(ProductEntity.BY_KILO, ProductType.FISH, 6.6f, 15);
		Product cheese = new Product(ProductEntity.BY_KILO, ProductType.CHEESE, 10.9f, 40);
		Product beer = new Product(ProductEntity.BY_ENTITY, ProductType.BEER, 1.2f, 20);
		Product chair = new Product(ProductEntity.BY_ENTITY, ProductType.CHAIR, 10, 60);
		Product book = new Product(ProductEntity.BY_ENTITY, ProductType.BOOK, 12, 50);
		
		shop.addProduct(meat);
		shop.addProduct(fish);
		shop.addProduct(cheese);
		shop.addProduct(beer);
		shop.addProduct(chair);
		shop.addProduct(book);
		shop.printProducts();
		
		ICustomer customer = new Customer(shop, 100.5f, 5);
		customer.addProductByKilo(meat, 5);
		customer.addProductByEntity(chair, 2);
		
		customer.removeProductByEntity(chair, 1);
		customer.pay();
		shop.printProducts();
		
		
		
		
		
		
	}

}
