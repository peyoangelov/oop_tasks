package shopTask;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Shop {
	private static final int INITIAL_AMOUT_OF_MONEY = 0;
	private String name = "";
	private String address = "";
	private float money;
	private Set<Product> products;

	public Shop(int capacity, String name, String address) {
		setName(name);
		setAddress(address);
		setMoney(INITIAL_AMOUT_OF_MONEY);
		this.products = new HashSet<Product>(capacity);
	}
	
	public void printProducts(){
		Iterator<Product> it = products.iterator();
		while(it.hasNext()){
		Product	current = it.next();
		System.out.println(current.getType()+" "+ current.getQuantity());
		}
	}

	public Product getProduct(Product product) {
		if (product != null) {
			Iterator<Product> it = products.iterator();
			while (it.hasNext()) {
				Product currentProduct = it.next();
				if (currentProduct.getType().equals(product.getType()))
					return currentProduct;
			}
		}
		return null;
	}

	public void addProduct(Product product) {
		if (product != null) {
			this.products.add(product);
			System.out.println(product.getType() + " has been added to " + this.getName());
		}
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		if (name != null) {
			this.name = name;
		}
	}

	public String getAddress() {
		return address;
	}

	private void setAddress(String address) {
		if (address != null)
			this.address = address;
	}

	public float getMoney() {
		return money;
	}

	public void setMoney(float money) {
		if (money >= 0)
			this.money = money;
	}

}
