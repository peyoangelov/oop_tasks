package shopTask;

import enums.ProductEntity;
import enums.ProductType;

public class Product {
	private ProductType type;
	private ProductEntity entity;
	private float price;
	private int quantity;

	public Product(ProductEntity entity, ProductType type, float price, int quantity) {
		this.setEntity(entity);
		this.setPrice(price);
		this.setType(type);
		this.setQuantity(quantity);
	}

	public ProductType getType() {
		return type;
	}

	private void setType(ProductType type) {
		if (type != null)
			this.type = type;
	}

	public ProductEntity getEntity() {
		return entity;
	}

	private void setEntity(ProductEntity entity) {
		if (entity != null)
			this.entity = entity;
	}

	public float getPrice() {
		return price;
	}

	private void setPrice(float price) {
		if (price > 0)
			this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		if (quantity >= 0)
			this.quantity = quantity;
	}

}
