package shopTask;

import java.util.HashMap;
import java.util.Map;

import interfaces.ICustomer;

public class Customer implements ICustomer {

	private Shop shop;
	private float money;
	private int maxProducts;
	private Map<Product, Integer> cart;

	public Customer(Shop shop, float money, int maxProducts) {
		this.setShop(shop);
		this.setMoney(money);
		this.setMaxProducts(maxProducts);
		this.cart = new HashMap<Product, Integer>(maxProducts);
	}

	@Override
	public void addProductByKilo(Product product, int quantity) {
		if (product != null && quantity > 0) {
			
			if (cart.containsKey(product)) {
				this.cart.put(product, this.cart.get(product) + quantity);
			} else {
				this.cart.put(product,quantity);
			}
			
			Product shopProduct = this.getShop().getProduct(product);
			if (shopProduct != null) {
				shopProduct.setQuantity(shopProduct.getQuantity() - quantity);
				System.out.println(this.getShop().getName() + " has decreased the quantity of " + product.getType()
						+ " to " + shopProduct.getQuantity());

			}
			System.out.println("Current user has added " + quantity + " of " + product.getType());

		}

	}

	@Override
	public void addProductByEntity(Product product, int quantity) {
		if (product != null && quantity > 0) {
			if (cart.containsKey(product)) {
				this.cart.put(product, this.cart.get(product) + quantity);
			} else {
				this.cart.put(product,quantity);
			}
			Product shopProduct = this.getShop().getProduct(product);
			if (shopProduct != null) {
				shopProduct.setQuantity(shopProduct.getQuantity() - quantity);
				System.out.println(this.getShop().getName() + " has decreased the quantity of " + product.getType()
						+ " to " + shopProduct.getQuantity());

			}
			System.out.println("Current user has added " + quantity + " of " + product.getType());

		}
	}

	@Override
	public void removeProductByKilo(Product product, int quantity) {
		if (product != null && quantity > 0) {
			if (this.cart.get(product) > quantity) {
				this.cart.put(product, this.cart.get(product) - quantity);
				Product shopProduct = this.getShop().getProduct(product);
				if (shopProduct != null) {
					shopProduct.setQuantity(shopProduct.getQuantity() + quantity);
					System.out.println(this.getShop().getName() + " has increased the quantity of " + product.getType()
							+ " to " + shopProduct.getQuantity());

				}
				System.out.println("Current user has removed " + quantity + " of " + product.getType());
			}
		}

	}

	@Override
	public void removeProductByEntity(Product product, int quantity) {
		if (product != null && quantity > 0) {
			if (this.cart.get(product) > quantity) {
				this.cart.put(product, this.cart.get(product) - quantity);
				Product shopProduct = this.getShop().getProduct(product);
				if (shopProduct != null) {
					shopProduct.setQuantity(shopProduct.getQuantity() + quantity);
					System.out.println(this.getShop().getName() + " has increased the quantity of " + product.getType()
							+ " to " + shopProduct.getQuantity());

				}
				System.out.println("Current user has removed " + quantity + " of " + product.getType());
			}
		}

	}

	@Override
	public void pay() {
		int totalAmount = 0;
		for (Product product : cart.keySet()) {
			totalAmount += cart.get(product) * product.getPrice();
		}
		if (this.getMoney() - totalAmount >= 0) {
			this.setMoney(this.getMoney() - totalAmount);
			this.getShop().setMoney(this.shop.getMoney() + totalAmount);
			System.out.println("This current customer has paid "+ totalAmount+ " to "+ this.getShop().getName());
		}

		else {
			System.out.println("Not enough money");
		}
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		if (shop != null)
			this.shop = shop;
	}

	public float getMoney() {
		return money;
	}

	public void setMoney(float money) {
		if (money > 0)
			this.money = money;
	}

	public int getMaxProducts() {
		return maxProducts;
	}

	public void setMaxProducts(int maxProducts) {
		if (maxProducts > 0)
			this.maxProducts = maxProducts;
	}

}
