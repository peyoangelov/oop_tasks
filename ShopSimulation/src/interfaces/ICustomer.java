package interfaces;

import shopTask.Product;

public interface ICustomer {
	public abstract void addProductByKilo(Product product, int quantity);
	public abstract void addProductByEntity(Product product, int quantity);
	public abstract void removeProductByKilo(Product product, int quantity);
	public abstract void removeProductByEntity(Product product, int quantity);
	public abstract void pay();
}
