package sweetShopTask;

import java.util.HashSet;
import java.util.Set;

public class Provider {
	
	@SuppressWarnings("unused")
	private String name = "";
	@SuppressWarnings("unused")
	private String phone = "";
	private Set<Order> orders;

public	Provider(String name, String phone) {
		if (name != null)
			this.name = name;
		if (phone != null && phone.length() == 10)
			this.phone = phone;
		this.orders = new HashSet<Order>();
	}

	public void deliverOrders() {
		for (Order order : orders) {
			deliverOrder(order);
		}
	}

	private void deliverOrder(Order order) {
		System.out.println("The provider is delivering  cakes ");		
		System.out.println(order.getPrice() + " has been paid to the sweetshop");	
		}

	public void addOrder(Order order) {
		orders.add(order);
	}

}
