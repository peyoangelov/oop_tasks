package sweetShopTask;

import java.util.ArrayList;
import java.util.List;

import enums.CakeType;
import enums.KidCakeType;
import enums.SpecialCakeType;
import enums.StandartCakeType;
import enums.WeddingCakeType;

public class PrivateClient extends Client {

	private static final double TIP_AMOUNT = 0.02;
	private List<Integer> vouchers;

	public PrivateClient(String name, String phone) {
		super(name, phone);
		generateVouchers();

	}

	private void generateVouchers() {
		int numberOfvauchers = 1 + (int) (Math.random() * ((4 - 1) + 1));

		this.vouchers = new ArrayList<Integer>(numberOfvauchers);
		System.out.println("Private client " + super.getName() + " has " + numberOfvauchers + " number of vauchers:");
		for (int index = 0; index < numberOfvauchers; index++) {
			int valueOfVoucher = 10 + (int) (Math.random() * ((30 - 10) + 1));
			vouchers.add(valueOfVoucher);
			System.out.print(valueOfVoucher + " $ , ");
		}

	}

	@Override
	public void placeOrder() {
		int numberOfCakes = 1 + (int) (Math.random() * ((3 - 1) + 1));
		Order order = new Order(0f, "Musagenica", "Monday");
		System.out.println("Private client " + super.getName() + " wants to order " + numberOfCakes + " cakes.");
		int totalPrice = 0;
		for (int index = 0; index < numberOfCakes; index++) {
			int cakeType = 1 + (int) (Math.random() * ((4 - 1) + 1));
			CakeType type = CakeType.values()[cakeType-1];
			Cake cakeToOrder = null;
			if (type.equals(CakeType.KIDCAKE)) {
				cakeToOrder = getShop().sellKidCake(KidCakeType.BIRTHDAY, "Gosho");

			}
			if (type.equals(CakeType.SPECIALCAKE)) {
				cakeToOrder = getShop().sellSpecialCake(SpecialCakeType.FIRMENA, "TeamBuilding");

			}
			if (type.equals(CakeType.STANDARTCAKE)) {
				cakeToOrder = getShop().sellStandartCake(StandartCakeType.SHOKOLADOVA, false);

			}
			if (type.equals(CakeType.WEDDINGCAKE)) {
				cakeToOrder = getShop().sellWeddingCake(WeddingCakeType.BIG, (byte) 20);

			}
			if (cakeToOrder == null) {
				System.out.println("There is no such cake ");
			} else {
				totalPrice += cakeToOrder.getPrice();
				order.addCake(cakeToOrder);
			}

		}
		int vouchersValue = 0;
		for (Integer voucher : vouchers) {
			while(vouchersValue <= totalPrice)
			vouchersValue += voucher;
		}
		totalPrice -= vouchersValue;
		order.setPrice(totalPrice);
		System.out.println("The price of the order is " + totalPrice);
		getShop().sentProvider().addOrder(order);

		float tip = (float) (order.getPrice() * TIP_AMOUNT);
		System.out.println(super.getName() + " has left " + tip + " $ for tips.");

	}
}
