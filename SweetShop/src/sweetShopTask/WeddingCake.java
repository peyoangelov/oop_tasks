package sweetShopTask;

import enums.WeddingCakeType;

public class WeddingCake extends Cake {

	private byte numberOfLevels;
	private WeddingCakeType type;

	public WeddingCake(String name, String description, float price, byte numberOfPieces, int numberOfCakes) {
		super(name, description, price, numberOfPieces, numberOfCakes);
		
	}

	public WeddingCakeType getType() {
		return type;
	}

	public void setNumberOfLevels(byte numberOfLevels) {
		this.numberOfLevels = numberOfLevels;
	}

	@Override
	public String toString() {
		super.toString();
		return "WeddingCake [numberOfLevels=" + numberOfLevels + ", type=" + type + "]";
	}

	public void setType(WeddingCakeType type) {
		this.type = type;
	}

	@Override
	public int compareTo(Cake o) {
		// TODO Auto-generated method stub
		return this.getNumberOfPieces()-o.getNumberOfPieces();
	}
	@Override
	Object getCakeSubType() {
		return this.getType();
	}


}
