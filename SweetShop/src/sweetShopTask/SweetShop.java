package sweetShopTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import enums.CakeType;
import enums.KidCakeType;
import enums.SpecialCakeType;
import enums.StandartCakeType;
import enums.WeddingCakeType;

public class SweetShop {
	@SuppressWarnings("unused")
	private String name = "";
	@SuppressWarnings("unused")
	private String address = "";
	@SuppressWarnings("unused")
	private String phone = "";
	private ArrayList<Provider> providers;
	private Map<CakeType, Set<Cake>> cakes;

	public SweetShop(String name, String address, String phone) {
		if (name != null)
			this.name = name;
		if (address != null)
			this.address = address;
		if (phone != null && phone.length() == 10)
			this.phone = phone;
		this.providers = new ArrayList<Provider>();
		this.cakes = new HashMap<CakeType, Set<Cake>>();
		addCake();
	}

	private void addCake() {
		cakes.put(CakeType.STANDARTCAKE, new TreeSet<Cake>());
		cakes.put(CakeType.KIDCAKE, new TreeSet<Cake>());
		cakes.put(CakeType.SPECIALCAKE, new TreeSet<Cake>());
		cakes.put(CakeType.WEDDINGCAKE, new TreeSet<Cake>());
	}

	public void addCake(Cake cake) {
		if (cake != null) {
			int cakeType = 1 + (int) (Math.random() * ((3 - 1) + 1));
			cake.setPrice(cake.getPrice()+cakeType);
			
			if (cake instanceof SpecialCake) {
				((SpecialCake) cake).setType(SpecialCakeType.values()[cakeType - 1]);
				cakes.get(CakeType.SPECIALCAKE).add(cake);
				updateCakePices(cake, CakeType.SPECIALCAKE);
				
			}
			if (cake instanceof KidCake) {
				((KidCake) cake).setType(KidCakeType.values()[cakeType - 1]);
				cakes.get(CakeType.KIDCAKE).add(cake);
				updateCakePices(cake, CakeType.KIDCAKE);

			}
			if (cake instanceof WeddingCake) {

				((WeddingCake) cake).setType(WeddingCakeType.values()[cakeType - 1]);
				cakes.get(CakeType.WEDDINGCAKE).add(cake);
				updateCakePices(cake, CakeType.WEDDINGCAKE);
			}
			if (cake instanceof StandartCake) {

				((StandartCake) cake).setType(StandartCakeType.values()[cakeType - 1]);
				cakes.get(CakeType.STANDARTCAKE).add(cake);
				updateCakePices(cake, CakeType.STANDARTCAKE);
			}
			System.out.println("A new " + cake.toString() + " has been added.");
			
		}

	}

	public Cake sellKidCake(KidCakeType type, String name) {
		KidCake kidCake = null;
		for (Cake cake : cakes.get(CakeType.KIDCAKE)) {
			if (cake.getCakeSubType().equals(type)) {
				if (cake.getNumberOfCakes() > 0) {
					kidCake = (KidCake) cake;
					kidCake.setKidsName(name);
					cake.setNumberOfCakes(cake.getNumberOfCakes() - 1);
					System.out.println("A new " + CakeType.KIDCAKE + " " + type + " has been sold.");
					return kidCake;
				} else {
					System.err.println("There are no kid cakes of type " + type);
					return null;
				}
			}
		}
		return kidCake;
	}

	public Cake sellSpecialCake(SpecialCakeType type, String name) {
		SpecialCake specialCake = null;
		for (Cake cake : cakes.get(CakeType.SPECIALCAKE)) {
			if (cake.getCakeSubType().equals(type)) {
				if (cake.getNumberOfCakes() > 0) {
					specialCake = (SpecialCake) cake;
					specialCake.setNameOfEvent(name);
					cake.setNumberOfCakes(cake.getNumberOfCakes() - 1);
					System.out.println("A new " + CakeType.SPECIALCAKE + " " + type + " has been sold.");
					return specialCake;
				} else {
					System.err.println("There are no special cakes of type " + type);
					return null;
				}
			}
		}
		return specialCake;
	}

	public Cake sellStandartCake(StandartCakeType type, boolean isSureped) {
		StandartCake standartCake = null;
		for (Cake cake : cakes.get(CakeType.STANDARTCAKE)) {
			if (cake.getCakeSubType().equals(type)) {
				if (cake.getNumberOfCakes() > 0) {
					standartCake = (StandartCake) cake;
					standartCake.setSureped(isSureped);
					cake.setNumberOfCakes(cake.getNumberOfCakes() - 1);
					System.out.println("A new " + CakeType.STANDARTCAKE + " " + type + " has been sold.");
					return standartCake;
				}
			} else {
				System.err.println("There are no standart cakes of type " + type);
				return null;
			}
		}
		return standartCake;
	}

	public Cake sellWeddingCake(WeddingCakeType type, byte numberOfLevels) {
		WeddingCake weddingCake = null;
		for (Cake cake : cakes.get(CakeType.WEDDINGCAKE)) {
		   if (cake.getCakeSubType().equals(type)) {
				if (cake.getNumberOfCakes() > 0) {
					weddingCake = (WeddingCake) cake;
					weddingCake.setNumberOfLevels(numberOfLevels);
					cake.setNumberOfCakes(cake.getNumberOfCakes() - 1);
					System.out.println("A new " + CakeType.WEDDINGCAKE + " " + type + " has been sold.");
					return weddingCake;
				} else {
					System.err.println("There are no wedding cakes of type " + type);
					return null;
				}
			}
		}
		return weddingCake;
	}

	public void addProvider(Provider provider) {
		providers.add(provider);
	}

	public Provider sentProvider() {
		int provider = 1 + (int) (Math.random() * ((providers.size() - 1) + 1));

		return providers.get(provider - 1);
	}

	public void executeOrders() {
		for (Provider provider : providers) {
			provider.deliverOrders();
		}
	}

	public void previewCatalog() {
		System.out.println("*************");
		for (CakeType type : cakes.keySet()) {
			// System.err.println(cakes.get(type).size());
			for (Cake cake : cakes.get(type)) {
				System.out.println(cake.toString() + " " + cake.getNumberOfCakes());
			}
		}
		System.out.println("*************");
	}
	
	private void updateCakePices(Cake cake, CakeType type){
		for(Cake currentCake:cakes.get(type)){
			if(currentCake.getCakeSubType().equals(cake.getCakeSubType())){
				currentCake.setNumberOfCakes(currentCake.getNumberOfCakes()+1);
			}
		}
	}
}
