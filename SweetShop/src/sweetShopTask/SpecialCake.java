package sweetShopTask;

import enums.SpecialCakeType;

public class SpecialCake extends Cake {


	private String nameOfEvent = "";
	private SpecialCakeType type;

	public SpecialCake(String name, String description, float price, byte numberOfPieces, int numberOfCakes) {
		super(name, description, price, numberOfPieces, numberOfCakes);
	}

	@Override
	public String toString() {
		super.toString();
		return "SpecialCake [nameOfEvent=" + nameOfEvent + ", type=" + type + "]";
	}
	public SpecialCakeType getType() {
		return type;
	}

	public void setNameOfEvent(String nameOfEvent) {
		this.nameOfEvent = nameOfEvent;
	}

	public void setType(SpecialCakeType type) {
		this.type = type;
	}

	@Override
	public int compareTo(Cake o) {
		// TODO Auto-generated method stub
		return (int) (this.getPrice()-o.getPrice());
	}
	
	@Override
	Object getCakeSubType() {
		return this.getType();
	}

}
