package sweetShopTask;

public abstract class Cake implements Comparable<Cake>{
	private String name = "";
	private String description = "";
	private float price;
	private byte numberOfPieces;
	private int numberOfCakes;

	public Cake(String name, String description, float price, byte numberOfPieces, int numberOfCakes) {
		if (name != null)
			this.name = name;
		if (description != null)
			this.description = description;
		if (price > 0)
			this.price = price;
		if (numberOfPieces > 0)
			this.numberOfPieces = numberOfPieces;
		if (numberOfCakes > 0)
			this.numberOfCakes = numberOfCakes;

	}

	public float getPrice() {
		return price;
	}

	public byte getNumberOfPieces() {
		return numberOfPieces;
	}

	public int getNumberOfCakes() {
		return numberOfCakes;
	}

	public void setNumberOfCakes(int numberOfCakes) {
		this.numberOfCakes = numberOfCakes;
	}

	@Override
	public String toString() {
		return "Cake [name=" + name + ", description=" + description + ", price=" + price + ", numberOfPieces="
				+ numberOfPieces + ", numberOfCakes=" + numberOfCakes + "]";
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	abstract Object getCakeSubType();
	
	

}
