package sweetShopTask;

import enums.CakeType;
import enums.KidCakeType;
import enums.SpecialCakeType;
import enums.StandartCakeType;
import enums.WeddingCakeType;

public class KoorporateClient extends Client {
	private static final double TIP_AMOUNT = 0.05;
	private final static float DISCOUNT = 0.1F;

	public KoorporateClient(String name, String phone) {
		super(name, phone);
	}

	public void placeOrder() {
		int numberOfCakes = 3 + (int) (Math.random() * ((5 - 3) + 1));
		System.out.println("Coorporate client " + super.getName() + " wants to order " + numberOfCakes + " cakes.");
		Order order = new Order(0f, "Musagenica", "Monday");
		int totalPrice = 0;
		for (int index = 0; index < numberOfCakes; index++) {
			int cakeType = 1 + (int) (Math.random() * ((4 - 1) + 1));
			CakeType type = CakeType.values()[cakeType-1];
			Cake cakeToOrder = null;
			if (type.equals(CakeType.KIDCAKE)) {
				cakeToOrder = getShop().sellKidCake(KidCakeType.BIRTHDAY, "Gosho");

			}
			if (type.equals(CakeType.SPECIALCAKE)) {
				cakeToOrder = getShop().sellSpecialCake(SpecialCakeType.FIRMENA, "TeamBuilding");

			}
			if (type.equals(CakeType.STANDARTCAKE)) {
				cakeToOrder = getShop().sellStandartCake(StandartCakeType.SHOKOLADOVA, false);

			}
			if (type.equals(CakeType.WEDDINGCAKE)) {
				cakeToOrder = getShop().sellWeddingCake(WeddingCakeType.BIG, (byte) 20);

			}
			if (cakeToOrder == null) {
				System.out.println("There are no such cake ");
			} else {
				totalPrice += cakeToOrder.getPrice();
				order.addCake(cakeToOrder);
			}

		}
		totalPrice -= totalPrice * DISCOUNT;
		order.setPrice(totalPrice);
		System.out.println("The price of the order is " + totalPrice);
		getShop().sentProvider().addOrder(order);
		float tip = (float) (order.getPrice() * TIP_AMOUNT);
		System.out.println(super.getName() + " has left " + tip + " $ for tips.");
	}

}
