package sweetShopTask;

import enums.KidCakeType;

public class KidCake extends Cake {

	private String kidsName = "";
	private KidCakeType type;

	public KidCake(String name, String description, float price, byte numberOfPieces, int numberOfCakes) {
		super(name, description, price, numberOfPieces, numberOfCakes);
		
	}

	public KidCakeType getType() {
		return type;
	}

	public void setKidsName(String kidsName) {
		if(kidsName != null)
		this.kidsName = kidsName;
	}

	@Override
	public String toString() {
		super.toString();
		return "KidCake [kidsName=" + kidsName + ", type=" + type + "]";
	}

	public void setType(KidCakeType type) {
		this.type = type;
	}

	@Override
	public int compareTo(Cake o) {
		return this.getNumberOfPieces()-o.getNumberOfPieces();
	}
	@Override
	public boolean equals(Object other) {
        KidCake cake = (KidCake) other;
        return this.getType().equals(cake.getType());
    }

	@Override
	Object getCakeSubType() {
		return this.getType();
	}
}
