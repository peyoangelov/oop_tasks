package sweetShopTask;

import enums.StandartCakeType;

public class StandartCake extends Cake {
	
	private boolean isSureped;
	private StandartCakeType type;
	
	public StandartCake(String name, String description, float price, byte numberOfPieces, int numberOfCakes) {
		super(name, description, price, numberOfPieces, numberOfCakes);
	
	}

	public StandartCakeType getType() {
		return type;
	}

	public void setSureped(boolean isSureped) {
		this.isSureped = isSureped;
	}

	@Override
	public String toString() {
		super.toString();
		return "StandartCake [isSureped=" + isSureped + ", type=" + type + "]";
	}

	public void setType(StandartCakeType type) {
		this.type = type;
	}

	@Override
	public int compareTo(Cake o) {
		// TODO Auto-generated method stub
		return (int) (this.getPrice()-o.getPrice());
	}
	@Override
	Object getCakeSubType() {
		return this.getType();
	}
	
	
}
