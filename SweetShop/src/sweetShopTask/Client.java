package sweetShopTask;

public abstract class Client {
	private String name = "";
	@SuppressWarnings("unused")
	private String phone = "";
	private SweetShop shop;
	
	Client(String name, String phone) {
		if (name != null)
			this.name = name;
		if (phone != null && phone.length() == 10)
			this.phone = phone;
		
	}

	public SweetShop getShop() {
		return shop;
	}
	public abstract void placeOrder();

	public String getName() {
		return name;
	}

	public void setShop(SweetShop shop) {
		this.shop = shop;
	}
	
	
}
