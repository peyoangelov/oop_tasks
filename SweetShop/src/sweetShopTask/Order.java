package sweetShopTask;

import java.util.HashSet;
import java.util.Set;

public class Order {
	private float price;
	@SuppressWarnings("unused")
	private String deliveryAddress = "";
	@SuppressWarnings("unused")
	private String dateDeliveryTime = "";
	private Set<Cake> cakes;

	Order(float price, String deliveryAddress, String dateDeliveryTime) {
		if (price > 0)
			this.price = price;
		if (deliveryAddress != null)
			this.deliveryAddress = deliveryAddress;
		if (dateDeliveryTime != null)
			this.dateDeliveryTime = dateDeliveryTime;
		this.cakes = new HashSet<Cake>();
	}
	public void addCake(Cake cake){
		cakes.add(cake);
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getPrice() {
		return price;
	}
	
	
}
