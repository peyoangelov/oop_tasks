package demoSweetShop;

import enums.KidCakeType;
import sweetShopTask.Cake;
import sweetShopTask.Client;
import sweetShopTask.KidCake;
import sweetShopTask.KoorporateClient;
import sweetShopTask.PrivateClient;
import sweetShopTask.Provider;
import sweetShopTask.SpecialCake;
import sweetShopTask.StandartCake;
import sweetShopTask.SweetShop;
import sweetShopTask.WeddingCake;


public class Demo {

	public static void main(String[] args) {
		SweetShop shop = new SweetShop("Sladki talanti", "Sofia ", "1234567891");
	Provider p1 = new Provider("provider1", "1234567891");
	Provider p2 = new Provider("provider2", "1234567891");
	
	shop.addProvider(p1);
	shop.addProvider(p2);
	createCakes(shop);
	Client privateClient =new PrivateClient("Peyo", "1234567891");
	Client coorporateClient = new KoorporateClient("Google", "1234567891");
	privateClient.setShop(shop);
	coorporateClient.setShop(shop);
	
	System.out.println();
	shop.previewCatalog();
	privateClient.placeOrder();
	coorporateClient.placeOrder();
	shop.previewCatalog();
	 	
		
	}

	
	public static void createCakes(SweetShop shop){
		
		for( int index =0 ; index< 30 ; index++){
		 int type=	1 + (int)(Math.random() * ((4 - 1) + 1));
		 Cake cake = null;
		 if( type == 1 ){
			  cake = new KidCake("bambi", "with milk", 16.5f, (byte) 8, 1);
		 }
		 if( type == 2 ){
			  cake = new SpecialCake("Special", "with almouds", 14.5f, (byte) 8, 1);
		 }
		 if( type == 3 ){
			  cake = new WeddingCake("Love", "with love", 22.5f, (byte) 80, 1);
		 }
		 if( type == 4 ){
			  cake = new StandartCake("French", "with nuts", 31.5f, (byte) 8, 1);
		 }
		 shop.addCake(cake);
		 
		 
			 
		}
	}
}
